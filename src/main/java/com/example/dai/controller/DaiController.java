package com.example.dai.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DaiController {

    @GetMapping("/ping")
    public boolean ping(){
        return true;
    }

    @GetMapping("/print")
    public String print(){
        return "DAI";
    }

    @GetMapping("/print02")
    public String print02(){
        return "DAI02";
    }
}
